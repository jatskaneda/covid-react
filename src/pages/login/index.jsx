import React, { Component } from "react";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import AccountCircle from "@material-ui/icons/AccountCircle";
import Button from "@material-ui/core/Button";
import InputAdornment from "@material-ui/core/InputAdornment";
import Typography from "@material-ui/core/Typography";
import History from "../../helpers/history";
import { ROUTES } from "../../helpers/routes";
import logo1 from "../../assets/images/CNE_logo.png";
import bg1 from "../../assets/images/0Czm58.jpg";

class index extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <div
          style={{
            height: "100%",
            width: "100%",
            backgroundImage: `url(${bg1})`,
            position: "absolute",
            backgroundSize: "cover",
            backgroundPosition: "center"
          }}
        >
          <Grid container justify="center">
            <Grid item xs={12} sm={12} md={5} lg={3} style={{ paddingTop: "30px" }}>
              <div style={{ padding: "20px" }}>
                <Grid container justify="center" spacing={3}>
                  <Grid item xs={12}>
                    <div style={{ textAlign: "center", paddingBottom: "50px" }}>
                      <img
                        src={logo1}
                        alt="Yuruary"
                        style={{
                          width: "100%",
                          height: "auto",
                          maxWidth: "350px"
                        }}
                      />
                    </div>

                    <Typography variant="h4" gutterBottom align="center" style={{ color: "black" }}>
                      Iniciar sesion
                    </Typography>
                  </Grid>
                  <Grid item xs={12}>
                    <TextField
                      label="Correo"
                      variant="outlined"
                      fullWidth
                      style={{ backgroundColor: "white" }}
                      InputProps={{
                        startAdornment: (
                          <InputAdornment position="start">
                            <AccountCircle />
                          </InputAdornment>
                        )
                      }}
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <TextField
                      variant="outlined"
                      label="Clave"
                      fullWidth
                      style={{ backgroundColor: "white" }}
                      InputProps={{
                        startAdornment: (
                          <InputAdornment position="start">
                            <AccountCircle />
                          </InputAdornment>
                        )
                      }}
                    />
                  </Grid>
                  <Grid item xs={12} style={{ marginTop: "20px" }}>
                    <div
                      style={{
                        padding: "15px"
                      }}
                    >
                      <Button
                        disableElevation
                        variant="contained"
                        fullWidth
                        color="primary"
                        style={{
                          margin: "0px",
                          height: "50px",
                          borderRadius: "50px"
                        }}
                        onClick={() => {
                          History.push(ROUTES.admin.home.path);
                        }}
                      >
                        Iniciar
                      </Button>
                    </div>
                  </Grid>
                </Grid>
              </div>
            </Grid>
          </Grid>
        </div>
      </div>
    );
  }
}

export default index;

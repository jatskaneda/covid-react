import React from "react";
import { Route, Switch, Router } from "react-router-dom";
import login from "./login";
import history from "../helpers/history";
import Layout from "../components/layout";
import homeAdmin from "./homeAdmin";
import usuarios from "./usuarios";
import { ROUTES } from "../helpers/routes";

// eslint-disable-next-line react/display-name
export default () => (
  <Router history={history}>
    <Switch>
      <Route exact path="/" component={login} />
      <Layout history={history}>
        <Route exact path={ROUTES.admin.home.path} component={homeAdmin} />
        <Route exact path={ROUTES.admin.users.path} component={usuarios} />
      </Layout>
    </Switch>
  </Router>
);

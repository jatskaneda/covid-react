import React, { Component } from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Slide from "@material-ui/core/Slide";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Select from "@material-ui/core/Select";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormHelperText from "@material-ui/core/FormHelperText";
import FormControl from "@material-ui/core/FormControl";

export default class componentName extends Component {
  constructor() {
    super();

    this.state = {
      tipoCedula: "",
      rol: ""
    };
  }

  render() {
    return (
      <div>
        <Dialog
          open={this.props.open}
          keepMounted
          onClose={this.props.handleClose}
          aria-labelledby="alert-dialog-slide-title"
          aria-describedby="alert-dialog-slide-description"
        >
          <DialogTitle id="alert-dialog-slide-title">{"Agregue un nuevo usuario al sistema"}</DialogTitle>
          <DialogContent>
            <Grid container spacing={3}>
              <Grid item xs={12} sm={6}>
                <TextField id="outlined-basic" label="Nombre" variant="outlined" fullWidth />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField id="outlined-basic" label="Apellido" variant="outlined" fullWidth />
              </Grid>
              <Grid item xs={12} sm={6}>
                <FormControl variant="outlined" fullWidth>
                  <InputLabel id="demo-simple-select-label">Tipo</InputLabel>
                  <Select
                    value={this.state.tipoCedula}
                    onChange={(v) => {
                      this.setState({ tipoCedula: v.target.value });
                    }}
                  >
                    <MenuItem value={0}>Venezolano</MenuItem>
                    <MenuItem value={1}>Extranjero</MenuItem>
                  </Select>
                </FormControl>
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField id="outlined-basic" label="Cedula" variant="outlined" fullWidth />
              </Grid>
              <Grid item xs={12} sm={6}>
                <FormControl variant="outlined" fullWidth>
                  <InputLabel id="demo-simple-select-label">Rol</InputLabel>
                  <Select
                    value={this.state.rol}
                    onChange={(v) => {
                      this.setState({ rol: v.target.value });
                    }}
                  >
                    <MenuItem value={0}>Admin</MenuItem>
                    <MenuItem value={1}>Extranjero</MenuItem>
                  </Select>
                </FormControl>
              </Grid>
            </Grid>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.props.handleClose} color="primary">
              Disagree
            </Button>
            <Button onClick={this.props.handleClose} color="primary">
              Agree
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

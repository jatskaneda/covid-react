import React, { Component } from "react";
import CustomTable from "../../components/customTable";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import CreateIcon from "@material-ui/icons/Create";
import HeaderText from "../../components/headerText";
import Fab from "@material-ui/core/Fab";
import NavigationIcon from "@material-ui/icons/Navigation";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import Formulario from "./formulario";

export default class Index extends Component {
  constructor() {
    super();

    this.state = {
      open: false
    };
  }

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  render() {
    return (
      <div>
        <HeaderText>Usuarios</HeaderText>
        <CustomTable
          columns={[
            { title: "Nombre y apellido", field: "name" },
            { title: "Empresa", field: "company" },
            { title: "Edad", field: "birth", type: "numeric" },
            { title: "Fecha de ingreso", field: "date" },
            { title: "Salario", field: "salary", type: "numeric" },
            { title: "Correo", field: "email", type: "numeric" },
            {
              field: "actions",
              Title: "Acciones",
              render: (rowData) => (
                <>
                  {" "}
                  <IconButton aria-label="delete">
                    <DeleteIcon />
                  </IconButton>
                  <IconButton aria-label="delete">
                    <CreateIcon />
                  </IconButton>
                </>
              )
            }
          ]}
          data={[
            {
              name: "Usuario test",
              company: "test",
              birth: "01/01/1999",
              date: "01/01/01",
              salary: "$333.33",
              email: "test@gmail.com"
            },
            {
              name: "Usuario test",
              company: "test",
              birth: "01/01/1999",
              date: "01/01/01",
              salary: "$333.33",
              email: "test@gmail.com"
            }
          ]}
          title=""
        />

        <Fab
          onClick={this.handleClickOpen}
          variant="extended"
          color="primary"
          style={{
            position: "fixed",
            zIndex: "55555",
            right: "29px",
            bottom: "28px"
          }}
        >
          <AccountCircleIcon style={{ marginRight: "20px" }} />
          Agregar usuario
        </Fab>

        <Formulario handleClickOpen={this.handleClickOpen} handleClose={this.handleClose} open={this.state.open} />
      </div>
    );
  }
}

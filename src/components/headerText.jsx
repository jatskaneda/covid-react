import React, { Component } from "react";
import Typography from "@material-ui/core/Typography";

export default class componentName extends Component {
  render() {
    return (
      <Typography variant="h4" gutterBottom style={{ color: "#373737" }}>
        {this.props.children}
      </Typography>
    );
  }
}

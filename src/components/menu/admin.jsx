import React, { Component } from "react";
import { List, ListItem, ListItemIcon, ListItemText } from "@material-ui/core";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import { ROUTES } from "../../helpers/routes";
import history from "../../helpers/history";
import AssessmentIcon from "@material-ui/icons/Assessment";
export default class Admin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openInv: false
    };

    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.setState((prevState) => ({ openInv: !prevState.openInv }));
  }

  render() {
    return (
      <List>
        <ListItem button onClick={() => history.push(ROUTES.admin.home.path)}>
          <ListItemIcon>
            <AssessmentIcon />
          </ListItemIcon>
          <ListItemText primary={ROUTES.admin.home.label} />
        </ListItem>
        <ListItem button onClick={() => history.push(ROUTES.admin.users.path)}>
          <ListItemIcon>
            <AccountCircleIcon />
          </ListItemIcon>
          <ListItemText primary={ROUTES.admin.users.label} />
        </ListItem>
      </List>
    );
  }
}
